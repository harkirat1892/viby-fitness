<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function __construct(){
		parent::__construct();

		// $this->fbAppID = '355663014773754';
		// $this->fbAppSecret = '92daaddfe22e50c1fff99d50e9eff407';
	}

	public function phpinfo(){
		echo phpinfo();
	}

	public function testMongo(){
		// $this->load->library('mongo_db', array('activate'=>'test'),'mongo_db2');
		// $this->load->library('mongo_db');
		$insertData = array("name"=> "harkirat", "cp"=>"18500");

		// var_dump($this->mongo_db->insert("test_users", $insertData));
		echo "<pre>";
		var_dump($this->mongo_db->where("_id", new mongoid("584bdf68ff8f9f35747b23db"))->get("test_users"));
		// var_dump($this->mongo_db->select("name")->like("dish_name", "Mojo Crepe", "i")->get("dishes"));
		// var_dump($this->mongo_db->where_in("finished", array(11))->get("pokemons"));
		// var_dump($this->mongo_db->where("_id", "57a45c8bc58ebb47048b4567")->get("pokemons"));
	}

	public function authentication(){
		$username = $this->input->post("username");
		$password = $this->input->post("password");

		if($username && $password){
			if($username == "harkiratisgod" && $password == "hehaha"){
				$this->session->set_userdata("isAuthorized", True);
				redirect(base_url()."index.php/backend");
			} else echo "Nope nope. That is wrong.";
		}else echo "Nothing received";
	}
	
	public function saveEmails(){
		if($this->input->post()){
			$email = trim($this->input->post("email"));

			$this->mongo_db->insert("mpower_emails", array("email"=> $email, "date"=> date("Y-m-d H:i:s")));
		} else show_404();
	}

	// sets owner_logged_in
	public function owner_login(){
		if($this->input->post()){
			$username = $this->input->post("username");
			$password = $this->input->post("password");

			if($username && $password){
				if($username == "account_18945" && $password == "pandarocks!"){
					$this->session->set_userdata("owner_logged_in", True);
					$this->session->set_userdata("recepient_id", "391978927607214");
					redirect(base_url()."index.php/dashboard");
				} else echo "Invalid username/password.";
			}
		} else{
			$this->load->view("login_dashboard");
		}
	}

	public function getFbData(){
		require_once APPPATH."libraries/facebook-sdk-v5/autoload.php";

		$fb = new Facebook\Facebook([
		  'app_id' => $this->fbAppID,
		  'app_secret' => $this->fbAppSecret,
		  'default_graph_version' => 'v2.5',
		]);

		$helper = $fb->getRedirectLoginHelper();
		$permissions = ['email', 'user_likes', 'manage_pages', 'public_profile', 'user_friends', 'pages_show_list']; // optional
		$loginUrl = $helper->getLoginUrl('https://mpower.chat/restaurant/index.php/test/callbackFB', $permissions);

		// echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';

		$data = array(
			"loginUrl" => $loginUrl
			);

		$this->load->view("fb_auth", $data);
	}

	public function callbackFB(){
		require_once APPPATH."libraries/facebook-sdk-v5/autoload.php";

		$fb = new Facebook\Facebook([
		  'app_id' => $this->fbAppID,
		  'app_secret' => $this->fbAppSecret,
		  'default_graph_version' => 'v2.5',
		]);


		$helper = $fb->getRedirectLoginHelper();
		try {
		  $accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  // When Graph returns an error
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  // When validation fails or other local issues
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}

		if (isset($accessToken)) {
		  // Logged in!
		  $_SESSION['facebook_access_token'] = (string) $accessToken;

		  // Now you can redirect to another page and use the
		  // access token from $_SESSION['facebook_access_token']
		}

		try {
		  // Returns a `Facebook\FacebookResponse` object
		  $response = $fb->get('/me?fields=id,name', $accessToken);
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}

		$user = $response->getGraphUser();

		try {
		  // Returns a `Facebook\FacebookResponse` object
		  $pageResponse = $fb->get('/me/accounts', $accessToken);
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}


		echo "<pre>";
		// var_dump($user);
		echo "<br><br><br>";
		var_dump($pageResponse->getDecodedBody());

		// $plainOldArray = $response->getDecodedBody();
		// var_dump($plainOldArray);

		// echo 'Name: ' . $user['name'];
	}

}
