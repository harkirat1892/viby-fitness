<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Messaging extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->supportEmail = "rescue@fitstreak.in";

		$this->witAccessToken = "R5CWSYPDBPGKGJCQNL66DEDQSTYEZJ66";
		$this->fbAccessToken = "EAACb3UxKDI8BANDZATbAUpLoTZABr8hNJhUaKPTJDwMMEnuHYziEWD2NWoTn45fNPXu6VrjbfNGw0rtBKKtZC3nsQvXg0Ixt2xLjNr7HjZCIxpXukv8i5NAyk7JLJQ0CZAnG5cGZABU7KUlqhjovKfj6wX7ggDZAAZAxqArXvEOrBgZDZD";

		$input = json_decode(file_get_contents('php://input'), true);
		$fbSenderID = $input['entry'][0]['messaging'][0]['sender']['id'];

		$this->fbSenderID = $fbSenderID ? $fbSenderID : False;
	}

	public function index() {
		show_404();
	}

	public function webhookThatHasToHasToBeSuperSecret123efgh321(){
		$input = json_decode(file_get_contents('php://input'), true);
		$fbSenderID = $this->fbSenderID;

		if($fbSenderID){
			echo "1";
			$receivedTimestamp = round( ($input['entry'][0]['time']) / 1000);
			// $this->mongo_db->insert("z_log_for_debug", array("fb_timestamp" => $receivedTimestamp, "strtotime_30"=> strtotime("30 minutes")));
			$this->mongo_db->insert("z_log_fb_inputs", array("timestamp"=> $receivedTimestamp, "fb_input"=> $input));


			// Test carousel
			// $this->sendTextResponseToUser("Initialising the video");
			// $exercises = array();

			// $exercises[] = array(
			// 	"_id" => 1,
			// 	"image" => "http://assets.bodybuilding.com/images/trackers/exercise/heatmap/7.gif",
			// 	"title" => "Hey I m here"
			// 	);
			// $exercises[] = array(
			// 	"_id" => 2,
			// 	"image" => "https://www.youtube.com/watch?v=6oLkcbpr30c",
			// 	"title" => "Hey I m here"
			// 	);
			// $exercises[] = array(
			// 	"_id" => 3,
			// 	"image" => "https://i.ytimg.com/vi/rgmGImhxkx0/hqdefault.jpg",
			// 	"title" => "Hey I m here"
			// 	);

			// $carouselElements = $this->packExercisesForCarousel($exercises);
			// $this->sendCarouselToUser($carouselElements);

			// $this->sendVideoResponseToUser("http://videocdn.bodybuilding.com/video/mp4/118000/118981m.mp4");

			// $this->sendTextResponseToUser("Sent the carousel");
			// die();

			// If user exists, fetches the data, else creates user
			$userData = $this->fbUserExistsCheck();
			$userID = $userData['user_id'];
			$awaitingResponse = trim($userData['response_expected_for']);
			$userExerciseInfo = $userData['info'];

			// $this->triggerRefreshOfExerciseInfo($userExerciseInfo);

			$this->mongo_db->where("_id", new mongoid($userID))->set("last_active_at", $receivedTimestamp)->update("users");

			$this->userData = $userData;

			$receivedMessage = trim($input['entry'][0]['messaging'][0]['message']['text']);
			$postbackPayload = trim($input['entry'][0]['messaging'][0]['postback']['payload']);
			$quickReplyPayload = trim($input['entry'][0]['messaging'][0]['message']['quick_reply']['payload']);


			if($awaitingResponse != '' && $receivedMessage != ''){
				if(strtolower($receivedMessage)=='cancel'){
					$this->mongo_db->where("_id", new mongoid($userID))->set("response_expected_for", "")->update("users");

					$this->sendTextResponseToUser("Input cancelled.");
					die();
				}
			} elseif($awaitingResponse != ''){
				$this->mongo_db->where("_id", new mongoid($userID))->set("response_expected_for", "")->update("users");
			}


			// special triggers
			if(strtolower($receivedMessage)=="start" || strtolower($receivedMessage)=="hi" || strtolower($receivedMessage)=="hey"){
				$postbackPayload = "initialise_chat";
			} elseif(strtolower($receivedMessage)=="help"){
				$postbackPayload = "initialise_help";
			}

			if($quickReplyPayload != '') {
				if(strpos($quickReplyPayload, "exercise@") !== false){
					$dataArray = explode("&", explode("@", $quickReplyPayload)[1]);

					if($dataArray[0]=="start"){
						$this->startExercise($userID, $dataArray[1]);
					} elseif($dataArray[0]=="info"){
						$this->showExerciseInfo($userID, $dataArray[1]);
					} elseif($dataArray[0]=="similar"){
						$this->showSimilarExercises($userID, $dataArray[1]);
					}
				}
				else{
					$this->sendTextResponseToUser($quickReplyPayload);
				}
			}
			elseif($postbackPayload != ''){
				if(strpos($postbackPayload, "exercise@") !== false){
					$data = explode("&", $postbackPayload);
				}
				else{
					switch ($postbackPayload) {
						case 'initialise_chat':
							$this->initialiseChatViaGetStarted();
							break;
							
						case 'initialise_help':
							$this->initialiseHelp();
							break;

						case 'show_mentor_program':
							$this->initialiseMentor();
							break;

						default:
							$this->sendTextResponseToUser($postbackPayload);
							break;
					}
				}
			} elseif($receivedMessage != ''){
				$responseToUser = json_decode($this->getWitEntities($receivedMessage));
				// $responseToUser['_text'] to compare if this is the text that was sent

				$entitiesFromWit = $responseToUser->entities;
				$this->mongo_db->insert("z_log_for_debug", array("entities_from_wit"=> $entitiesFromWit));

				foreach ($entitiesFromWit as $entity => $value) {
					if($entity == "abusive_words"){
						$this->sendTextResponseToUser("No swearing please? Even though I'm a robot, but if I get disrespected, it keeps me awake at nights :(");
						die();
					}elseif($entity=="intent"){
						foreach ($value as $entityValue) {
							if($value[0]->value=="greeting"){
								// $this->sendTextResponseToUser("Hey!! :D");
								$this->initialiseChatViaGetStarted();
								die();
							}elseif($value[0]->value=="motivation_dose" || $value[0]->value=="ditch_exercise"){
								$this->sendRandomQuote();
								die();
							}elseif($value[0]->value=="about_bot"){
								$this->sendTextResponseToUser("I'm Viby, your personal chat assistant!");
								die();
							}
						}
					}elseif($entity == "emoticon"){
						if($value[0]->value=="sad" || $value[0]->value=="disappointed" || $value[0]->value=="cry"){
							$this->sendTextResponseToUser("We're in this together, ".$this->userData['first_name']."!\n\n\nBad times are just to give you an excuse to get into your beast mode!");

							$this->sendRandomQuote();
						}else{
							$this->sendTextResponseToUser($receivedMessage);
						}
						die();
					}
				}

				$this->sendTextResponseToUser("Sorryyy! Failed to understand you :(\n\nPlease try again in more simpler words?");
				die();
			}
		} elseif($this->input->get("hub_mode")=="subscribe" && $this->input->get("hub_verify_token")=="hey_there_harkirat_on_the_door"){
			echo $this->input->get("hub_challenge");
		} else{
			show_404();
			// echo "hey";
		}
	}

	private function sendRandomQuote(){
		$quotes = array(
			"Excuses burn zero calories.",
			"Motivation is what gets you started. Habit is what keeps you going. \n\n- Jim Ryun",
			"Yesterday I was clever, so I wanted to change the world. Today I am wise, so I am changing myself. \n\n- Rumi",
			"A great future doesn’t require a great past. \n\n- William Chapman",
			"Float like a butterfly, sting like a bee. \n\n- Muhammad Ali"
			);

		$quote = $quotes[array_rand($quotes)];
		$this->sendTextResponseToUser($quote);
	}

	// If user exists already, returns user's userID
	// Else creates the user account and as expected, returns the new userID
	private function fbUserExistsCheck(){
		$fbSenderID = $this->fbSenderID;
		$userExists = $this->mongo_db->where(array("fb_user_id"=> $fbSenderID))->get("users");

		// If a user exists, give back the ID and name
		if($userExists)
			return array("user_id"=> (string)$userExists[0]['_id'], "first_name" => explode(' ', $userExists[0]['name'])[0], "full_name" => $userExists[0]['name'], "response_expected_for"=> $userExists[0]['response_expected_for'], "info" => $userExists[0]['info']);

		if(!$userExists && $fbSenderID){
			$ch = curl_init('https://graph.facebook.com/v2.7/'.$fbSenderID.'/?fields=first_name,last_name,profile_pic,locale,timezone,gender&access_token='.$this->fbAccessToken);
			curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 0);
			$userData = json_decode(curl_exec($ch));
			curl_close($ch);

			if($userData){
				$defaultInfo = array(
						"weight" => False,
						"height" => False,
						"is_veg" => False,
						"level" => 1,
						"goal_id" => False,
						"idol_id" => False,
						"persona_id" => False,
						// "mentor_fb_id" => substr(uniqid('', true), -5)
						"mentor_fb_id" => substr(md5(microtime()),rand(0,26),5)
						);

				$addUserData = array(
					"fb_user_id" => (string)$fbSenderID,
					"name" => trim($userData->first_name." ".$userData->last_name),
					"profile_pic" => $userData->profile_pic,
					"gender"=> $userData->gender,
					"timezone" => $userData->timezone,
					"locale" => $userData->locale,
					"info" => $defaultInfo,
					"response_expected_for"=>'',
					"last_active_at" => strtotime()
					);
				return array("user_id"=> $this->mongo_db->insert("users", $addUserData), "first_name"=> $userData->first_name, "full_name" => $addUserData['name'], "response_expected_for"=> '', "info" => False);
			} else return False;
		} else return False;
	}

	private function sendTextResponseToUser($answer=False, $quickReplies=False){
		$fbSenderID = $this->fbSenderID ? $this->fbSenderID : False;

		if(!$answer)
			$answer = "Something bad happened :(";

		if(!$quickReplies){
			$response = [
			    'recipient' => [ 'id' => $fbSenderID ],
			    'message' => [ 'text' => $answer ]
			];
		} else{
			$response = [
			    'recipient' => [ 'id' => $fbSenderID ],
			    'message' => [ 'text' => $answer, 'quick_replies' => $quickReplies ],
			];
		}

		return $this->responseDispatcher($response);
	}

	private function sendVideoResponseToUser($videoURL = False){
		$fbSenderID = $this->fbSenderID ? $this->fbSenderID : False;

		if(!$videoURL)
			$this->sendTextResponseToUser("Ooops. Something bad happened!");

		$response = [
		    'recipient' => [ 'id' => $fbSenderID ],
		    'message' => [ 'attachment' => array(
		    		"type" => "video",
		    		"payload" => array("url"=> $videoURL)
		    	)
			]
		];

		return $this->responseDispatcher($response);
	}

	private function sendCarouselToUser($carouselElements=False){
		$fbSenderID = $this->fbSenderID ? $this->fbSenderID : False;

		if(!$carouselElements){
			$this->sendTextResponseToUser("Couldn't generate carousel. Please report this to ".$this->supportEmail."");
			die();
		}
		$response = [
		    'recipient' => [ 'id' => $fbSenderID ],
		    'message' => array(
		    	"attachment" => array(
		    		"type"=> "template",
		    		"payload"=>array(
		    			"template_type"=> "generic",
		    			"elements"=> $carouselElements
		    			)
		    		)
		    	)
		];

		return $this->responseDispatcher($response);
	}

	private function sendButtonToUser($buttonText=False, $buttons=False){
		$fbSenderID = $this->fbSenderID ? $this->fbSenderID : False;

		if($buttonText !='' && $buttons && $fbSenderID){
			$response = [
			    'recipient' => [ 'id' => $fbSenderID ],
			    'message' => [
			    	"attachment" => [
			    		"type"=> "template",
			    		"payload"=> [
			    			"template_type"=> "button",
			    			"text"=> $buttonText,
			    			"buttons"=> $buttons
		    			]
		    		]
		    	]
			];

			return $this->responseDispatcher($response);
		} else{
			$this->sendTextResponseToUser("Couldn't generate buttons. Please report this to ".$this->supportEmail."");
			die();
		}
	}

	private function sendTypingDotsToUser($sleepTime=5){
		$fbSenderID = $this->fbSenderID ? $this->fbSenderID : False;

		if($fbSenderID){
			$response = ['recipient' => [ 'id' => $fbSenderID ],'sender_action' => 'typing_on'];

			$this->responseDispatcher($response);
			sleep($sleepTime);
		}
	}

	private function responseDispatcher($response){
		$ch = curl_init('https://graph.facebook.com/v2.7/me/messages?access_token='.$this->fbAccessToken);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		$result = curl_exec($ch);
		curl_close($ch);

		if($result) return True;
		else return False;
	}

	private function packExercisesForCarousel($exercises=False){
		if($exercises){
			$carouselElements = array();

			foreach ($exercises as $ex) {
				$buttons = [
						[
							"type"=> "postback",
			                "title"=> "Start",
			                "payload"=> "exercise@start&".(string)$ex['_id']
		                ],
						[
							"type"=> "postback",
			                "title"=> "Info",
			                "payload"=> "exercise@info&".(string)$ex['_id']
		                ],
		                [
							"type"=> "postback",
			                "title"=> "Similar Exercises",
			                "payload"=> "exercise@similar&".(string)$ex['_id']
		                ]
		            ];

				$subtitle = "Hey";

				$imageURL = $ex['image'];

				$element=array(
					"title"=> $ex['title'],
					"image_url"=> $imageURL,
		            "subtitle"=> $subtitle,
		            "buttons"=> $buttons
					);

				$carouselElements[] = $element;
			}
			return $carouselElements;
		}else{
			if(count($exercises)>10){
				$this->sendTextResponseToUser("Too many results, I fell down under the weight :(");
			}
			else $this->sendTextResponseToUser("Packaging process failed.");
		}
	}

	// Returns the entities like intent, food_type for the text supplied
	private function getWitEntities($text=False){
		if(!$text)
			return False;

		$text = urlencode(trim($text));
		$witURL = "https://api.wit.ai/message?v=20160526&q=".$text;

		$ch = curl_init($witURL);
		curl_setopt($ch, CURLOPT_POST, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Authorization: Bearer '.$this->witAccessToken]);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}

	private function initialiseChatViaGetStarted(){
		$buttonText = "Hey ".$this->userData['first_name']."!\n\nYour personal fitness trainer, Viby is glad to see you!";

		$buttons = [
			[
				"type"=> "postback",
				"title"=> "Let's Be Friends",
				"payload"=> "huh"
			],
			[
				"type"=> "postback",
				"title"=> "Help",
				"payload"=> "initialise_help"
			]
		];
		$this->sendButtonToUser($buttonText, $buttons);

		$this->setPersistentMenu();
		$this->setGreetingButton();
		$this->setGreetingText();
	}

	private function initialiseHelp(){
		$this->sendTextResponseToUser("We will..\n\nwe will...\n\nHELP YOU!\n\n\n\nDOOM DOOOM THAKK!");
	}

	// One Time Thingies
	/*
		Sets persistent menu
		Needs to be set only once
	 */
	private function setPersistentMenu(){
		$buttons = array();

	    // $buttons[] = [
	    //   "type"=>"postback",
	    //   "title"=>"Get Started",
	    //   "payload"=>"initialise_chat"
	    // ];
	    $buttons[] = [
	      "type"=>"postback",
	      "title"=>"Mentor",
	      "payload"=>"show_mentor_program"
	    ];
	    $buttons[] = [
	      "type"=>"postback",
	      "title"=>"Help",
	      "payload"=>"initialise_help"
	    ];
	    $buttons[] = [
	      "type"=>"postback",
	      "title"=>"Get Exercise Schedule",
	      "payload"=>"show_exercise_schedule"
	    ];
	    // $buttons[] = [
	    //   "type"=>"web_url",
	    //   "title"=>"View Website",
	    //   "url"=>"https://fitstreak.in"
	    // ];

		$response = [
			"setting_type" => "call_to_actions",
			"thread_state" => "existing_thread",
			"call_to_actions"=>$buttons
		];

		$ch = curl_init('https://graph.facebook.com/v2.7/me/thread_settings?access_token='.$this->fbAccessToken);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		$result = curl_exec($ch);
		curl_close($ch);
		var_dump($result);
	}

	private function setGreetingText(){
		$response = [
			"setting_type" => "greeting",
			"thread_state" => "new_thread",
			"greeting"=> ["text"=>"I may never be the biggest, nor the strongest, but I can always be the bravest."]
		];

		$ch = curl_init('https://graph.facebook.com/v2.7/me/thread_settings?access_token='.$this->fbAccessToken);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		$result = curl_exec($ch);
		curl_close($ch);
		var_dump($result);
	}

	private function setGreetingButton(){
		$response = [
			"setting_type" => "call_to_actions",
			"thread_state" => "new_thread",
			"call_to_actions"=> [["payload" => "initialise_chat"]]
		];

		$ch = curl_init('https://graph.facebook.com/v2.7/me/thread_settings?access_token='.$this->fbAccessToken);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($response));
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
		$result = curl_exec($ch);
		curl_close($ch);
		var_dump($result);
	}

	// once a time functions end here

	private function initialiseMentor(){
		if($this->userData['info']['mentor_fb_id']){
			$mentorData = $this->mongo_db->where("fb_user_id", $this->userData['info']['mentor_fb_id'])->get("users");

			$this->sendTextResponseToUser("You have already chosen ".$mentorData[0]['name']." as your mentor. Stick to that, buddy!");
		} else{

		}
	}

	// private function triggerRefreshOfExerciseInfo($userExerciseInfo=False){
	// 	if(!$userExerciseInfo){
	// 		$this->sendTextResponseToUser("Hey bud! To help serve you without being called a useless bot, I need some info about you. Yes, PINKY PROMISE, it will always stay personal between you and me ONLY.\n\nLet's start the journey, shall we?! :D");

	// 		// "weight" => False,
	// 		// "height" => False,
	// 		// "is_veg" => False,
	// 		// "level" => 1,
	// 		// "goal_id" => False,
	// 		// "idol_id" => False,
	// 		// "persona_id" => False,

	// 		// $this->mongo_db->where("fb_user_id", $this->fbSenderID)->set("response_expected_for", "weight_in_kg")->update("users");



	// 		die();			
	// 	}

	// 	// if()
	// }

}